let current_matrix = [];

// Função para gerar string no formato de matriz, itera sobre cada item, somando na variável str.
function generateMatrixString(matrix, str = "") {
  for (v in matrix) {
    str += "[";
    for (u in matrix[v]) {
      str += `${matrix[v][u]}, `;
    }
    str += "],\n";
  }
  return str;
}

function addGraph(matrix) {
  const list = document.getElementById("graph");
  if (list.hasChildNodes()) {
    list.removeChild(list.children[0]);
  }

  const para = document.createElement("div");
  para.innerText = generateMatrixString(matrix);
  console.log(matrix);
  document.getElementById("graph").appendChild(para);
}

function generate_stars() {
  const matrix = [];
  const stars_qt = document.getElementById("stars").value;
  // Criando matriz
  for (let i = 0; i < stars_qt; i++) {
    matrix[i] = [];
    for (let j = 0; j < stars_qt; j++) {
      // para cada iteração do laço, verifica se o indice i e j são iguais, se forem sabemos que
      // a mesma estrela está sendo referenciada, então o valor será igual a 0.
      if (i == j) {
        matrix[i][j] = 0;
      } else {
        // No caso em que os dois indices são diferentes podemos definir as ligações entre as estrelas de forma aleatória.
        // A aleatoriedade é feita a partir do metodo random(), gerando 0 ou 1.
        matrix[i][j] = Math.floor(Math.random() * 2);
      }
    }
  }

  // Percorrendo a matriz para ajustar o grafo para o modelo direcionado (Se uma estrela x tem conexão com a estrela y, a estrela y necessariamente terá conexão com x).
  for (let i in matrix) {
    for (let j in matrix[i]) {
      // Quando uma posição na matriz tem valor igual a 1, obrigatoriamente a posição inversa também terá valor 1.
      if (matrix[i][j] == 1) {
        matrix[j][i] = 1;
      }
    }
  }

  addGraph(matrix);
  current_matrix = matrix;
}

function check_connection() {
  const s1 = document.getElementById("star1").value;
  const s2 = document.getElementById("star2").value;

  const list = document.getElementById("result");
  if (list.hasChildNodes()) {
    list.removeChild(list.children[0]);
  }

  const para = document.createElement("div");

  // Verificar se na posição da estrela 1 e 2 o valor contido é 1, se for existe ligação.
  if (current_matrix[s1 - 1][s2 - 1] == 1) {
    para.innerText = "Há ligação";
    document.getElementById("result").appendChild(para);
    document.getElementById("result").className = "color-green";
  } else {
    para.innerText = "Não há ligação";
    document.getElementById("result").appendChild(para);
    document.getElementById("result").className = "color-red";
  }
}
